# Store Manager

#### by Óscar Ousinde Suárez

This is an Single Page Aplication (SPA) build for the management of a store. This time I used the [Fake Store API](https://fakestoreapi.com/), wich provided me the products of the store.

This API has it's limitations so I had to sligly modificate the data to achieve my desiree results.

#### Structure of the API

- A Home page where you can find all the products.
- A Header and a Footer are available in all the pages of the app.
- A page where you can find the products by category.
- A page where you can find the detail of each product.
- A manager page. Here you can add, edit and delete products.

#### Availability of the project

- You can find my project in [Gitlab](https://gitlab.com/OscarOusinde/final-task).
- At [DockerHub](https://hub.docker.com/repository/docker/ososcoder/final-task/general) as an image.
- Hosted here [Store Manager](https://shop-manager-38cb4.firebaseapp.com/).

#### Technologies used

- Docker.
- Web Components (JS Vanilla and LitElement).
- Testing with Jest.
- Testing with Cypress.
