describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    //Visit a product and back Home
    cy.get(
      ':nth-child(1) > .mainStore__productsList__linkProduct > product-card > .mainStore__productsList__linkProduct__cardProduct'
    ).click();
    cy.get('.headerStore__containerLogoMenu__logo').click();

    //Visit a category, visit a product and back Home
    cy.get(
      '.headerStore__navigation__navListUl__li > .headerStore__navigation__navListUl__li__link'
    ).click();
    cy.get(
      ':nth-child(2) > .categoriesSection__productsList__linkProduct > product-card > .mainStore__productsList__linkProduct__cardProduct'
    ).click();
    cy.get('.headerStore__containerLogoMenu__logo').click();

    //Visiting Manager
    cy.get('.headerStore__navigation__managerLink').click();

    //Adding new product, visiting it and back Home
    cy.get('.managerEditor__form__inputs__name').type('New product');
    cy.get('.managerEditor__form__inputs__price').type('1000');
    cy.get('.managerEditor__form__inputs__description').type(
      'New product description. Testing with Cypress'
    );
    cy.get('.managerEditor__form__inputs__category').type('electronics');
    cy.get('.managerEditor__form__inputs__image').type(
      'https://media.wired.com/photos/593365fda88f414d9a8c7e34/master/w_580,h_452,c_limit/4175840101_5f98070d27_o.jpeg'
    );
    cy.get('.managerEditor__form__buttons__add').click();
    cy.wait(1500);
    cy.get('.headerStore__containerLogoMenu__logo').click();
    cy.get(
      ':nth-child(1) > .mainStore__productsList__linkProduct > product-card > .mainStore__productsList__linkProduct__cardProduct'
    ).click();
    cy.get('.singleProductDisplay__data__productName').contains('New product');
    cy.get('.headerStore__containerLogoMenu__logo').click();
    cy.wait(1500);

    //Deleting a product and back Home
    cy.get('.headerStore__navigation__managerLink').click();
    cy.get(
      ':nth-child(1) > .managerList__productslist__product__deleteButton'
    ).click();
    cy.get('.headerStore__containerLogoMenu__logo').click();
    cy.get(
      ':nth-child(1) > .mainStore__productsList__linkProduct > product-card > .mainStore__productsList__linkProduct__cardProduct'
    ).click();
    cy.get('.singleProductDisplay__data__productName').contains(
      'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops'
    );
    cy.get('.headerStore__containerLogoMenu__logo').click();

    //Modifying a product, checking it and back home
    cy.get('.headerStore__navigation__managerLink').click();
    cy.get('.managerList__productslist__product').first().click();
    cy.get('.managerEditor__form__inputs__name').clear().type('Mochila');
    cy.get('.managerEditor__form__buttons__modify').click();
    cy.wait(1500);
    cy.get('.headerStore__containerLogoMenu__logo').click();
    cy.get(
      ':nth-child(1) > .mainStore__productsList__linkProduct > product-card > .mainStore__productsList__linkProduct__cardProduct'
    ).click();
    cy.get('.singleProductDisplay__data__productName').contains('Mochila');
    cy.get('.headerStore__containerLogoMenu__logo').click();
  });
});
