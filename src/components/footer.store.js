import { LitElement, html } from 'lit';

export class FooterStore extends LitElement {
  render() {
    return html`
      <footer class="footerStore">
        <p class="footerStore__copy">&copyShop Manager</p>
        <p class="footerStore__rights">All rights reserved to ososCoder</p>
        <a
          href="https://gitlab.com/OscarOusinde"
          class="footerStore__personalLinks"
        >
          <img
            src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original.svg"
          />
        </a>
        <img />
      </footer>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('footer-store', FooterStore);
