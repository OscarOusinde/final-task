import { LitElement, html } from 'lit';
import { productsList, productsCategory } from '../states/state';
import logo from '../assets/logos/logo.png';
import menuIcon from '../assets/icons/menu-icon.svg';
import menuClose from '../assets/icons/close-icon.svg';

export class HeaderStore extends LitElement {
  static get properties() {
    return {
      productsList: { type: Array },
    };
  }

  render() {
    return html` <header class="headerStore">
      <div class="headerStore__containerLogoMenu">
        <a href="/"
          ><img src="${logo}" class="headerStore__containerLogoMenu__logo"
        /></a>
        <img
          src="${menuIcon}"
          class="headerStore__containerLogoMenu__menuIcon"
          @click="${this.openMenu}"
        />
      </div>
      <nav class="headerStore__navigation">
        <img
          src="${menuClose}"
          alt="Icon for closing menu"
          class="headerStore__navigation__closeMenu"
          @click="${this.closeMenu}"
        />
        <ul class="headerStore__navigation__navListUl">
          <li class="headerStore__navigation__navListUl__li">
            <a
              href="/products"
              @click="${this.goToCategory}"
              class="headerStore__navigation__navListUl__li__link"
              >Electronics</a
            >
          </li>
          <li>
            <a
              href="/products"
              @click="${this.goToCategory}"
              class="headerStore__navigation__navListUl__li__link"
              >Jewelery</a
            >
          </li>
          <li>
            <a
              href="/products"
              @click="${this.goToCategory}"
              class="headerStore__navigation__navListUl__li__link"
              >Men's clothing</a
            >
          </li>
          <li>
            <a
              href="/products"
              @click="${this.goToCategory}"
              class="headerStore__navigation__navListUl__li__link"
              >Women's clothing</a
            >
          </li>
        </ul>
        <a href="/manager" class="headerStore__navigation__managerLink"
          >Manager</a
        >
      </nav>
    </header>`;
  }

  closeMenu() {
    const closeIcon = document.querySelector(
      '.headerStore__navigation__closeMenu'
    );

    const nav = document.querySelector('.headerStore__navigation--open');

    closeIcon.addEventListener('click', () => {
      nav.classList.remove('headerStore__navigation--open');
      nav.classList.add('headerStore__navigation');
    });
  }

  openMenu() {
    const menuIcon = document.querySelector(
      '.headerStore__containerLogoMenu__menuIcon'
    );

    const nav = document.querySelector('.headerStore__navigation');

    menuIcon.addEventListener('click', () => {
      nav.classList.remove('headerStore__navigation');
      nav.classList.add('headerStore__navigation--open');
    });
  }

  goToCategory(e) {
    const category = e.target.textContent.toLowerCase();

    //state
    this.productsList = productsList.products;

    const categoryProducts = this.productsList?.filter(
      (product) => product.category === category
    );

    //State
    productsCategory.products = categoryProducts;

    //Close nav
    const nav = document.querySelector('.headerStore__navigation--open');
    if (nav) {
      nav.classList.remove('headerStore__navigation--open');
      nav.classList.add('headerStore__navigation');
    }
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('header-store', HeaderStore);
