import { LitElement, html } from 'lit';
import { AllProductsUseCase } from '../usecases/all-products.usecase';
import { productsList, singleProduct } from '../states/state';
import { snapshot } from 'valtio';
import '../ui/product.card';

export class MainStore extends LitElement {
  static get properties() {
    return {
      productsList: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    //State
    const products = snapshot(productsList);

    if (products.products.length === 0) {
      this.productsList = await AllProductsUseCase.execute();

      productsList.products = this.productsList;
    } else {
      this.productsList = products.products;
    }
  }

  render() {
    return html` <h1 class="mainStoreTitle">All products</h1>
      <section class="mainStore">
        <ul class="mainStore__productsList">
          ${this.productsList?.length > 0
            ? this.productsList?.map(
                (product) =>
                  html`<li>
                    <a
                      href="/product"
                      data-product="${JSON.stringify(product)}"
                      @click="${this.gotToProduct}"
                      class="mainStore__productsList__linkProduct"
                    >
                      <product-card .product=${product}></product-card>
                    </a>
                  </li>`
              )
            : html`<li>Not products available</li>`}
        </ul>
      </section>`;
  }

  gotToProduct(e) {
    const product = JSON.parse(e.currentTarget.dataset.product);

    //State
    singleProduct.product = product;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('main-store', MainStore);
