import { LitElement, html } from 'lit';
import { productsList } from '../states/state';
import { snapshot } from 'valtio';
import { AddProductUseCase } from '../usecases/add-product.usecase';
import { UpdateProductUseCase } from '../usecases/update-product.usecase';

export class ProductEditor extends LitElement {
  static get properties() {
    return {
      productSelected: { type: Object },
    };
  }

  render() {
    return html`
      <section class="managerEditor">
        <h2 class="managerEditor__title">Product editor</h2>
        <form class="managerEditor__form">
          <fieldset class="managerEditor__form__inputs">
            <label for="productName" class="managerEditor__form__inputs__label"
              >Product Name</label
            >
            <input
              name="productName"
              type="text"
              placeholder="Insert the name of the product"
              required
              .value="${this.productSelected?.productName
                ? this.productSelected?.productName
                : ''}"
              class="managerEditor__form__inputs__name"
            />
            <label for="price" class="managerEditor__form__inputs__label"
              >Price ($)</label
            >
            <input
              name="price"
              type="number"
              min="0"
              placeholder="Insert the price of the product"
              required
              .value="${this.productSelected?.price}"
              class="managerEditor__form__inputs__price"
            />
            <label for="description" class="managerEditor__form__inputs__label"
              >Description</label
            >
            <textarea
              name="description"
              required
              placeholder="Insert a description for the product"
              .value="${this.productSelected?.description
                ? this.productSelected?.description
                : ''}"
              class="managerEditor__form__inputs__description"
            ></textarea>
            <label for="category" class="managerEditor__form__inputs__label"
              >Category
              <span class="managerEditor__form__inputs__label--categories"
                >(electronics, jewelery, men's clothing, women's clothing)</span
              ></label
            >
            <select
              name="category"
              required
              .value="${this.productSelected?.category}"
              class="managerEditor__form__inputs__category"
            >
              <option value="">--Please choose an option--</option>
              <option value="electronics">Electronics</option>
              <option value="jewelery">Jewelery</option>
              <option value="men's clothing">Mens's clothing</option>
              <option value="women's clothing">Women's clothing</option>
            </select>
            <label form="image" class="managerEditor__form__inputs__label"
              >Imagen URL</label
            >
            <input
              name="image"
              type="text"
              required
              placeholder="Insert an URL of the product"
              .value="${this.productSelected?.image
                ? this.productSelected?.image
                : ''}"
              class="managerEditor__form__inputs__image"
            />
          </fieldset>
          <fieldset class="managerEditor__form__buttons">
            <button
              @click="${this.cleanInputs}"
              class="managerEditor__form__buttons__clean"
            >
              Clean editor
            </button>
            <button
              @click="${this.modifyProduct}"
              class="managerEditor__form__buttons__modify"
            >
              Modify
            </button>
            <button
              @click="${this.addProduct}"
              class="managerEditor__form__buttons__add"
            >
              Add
            </button>
          </fieldset>
        </form>
      </section>
    `;
  }

  async modifyProduct(e) {
    e.preventDefault();
    const productName = document.getElementsByName('productName')[0].value;
    const price = document.getElementsByName('price')[0].value;
    const description = document.getElementsByName('description')[0].value;
    const category = document.getElementsByName('category')[0].value;
    const image = document.getElementsByName('image')[0].value;

    const productUpdate = {
      id: this.productSelected.id,
      productName,
      price,
      description,
      category,
      image,
      rating: this.productSelected.rating,
    };

    //State
    const products = snapshot(productsList);

    const updatedProductsList = await UpdateProductUseCase.execute(
      productUpdate,
      products.products
    );

    //State
    productsList.products = [...updatedProductsList];

    this.cleanInputs(e);
  }

  async addProduct(e) {
    e.preventDefault();
    const productName = document.getElementsByName('productName')[0].value;
    const price = document.getElementsByName('price')[0].value;
    const description = document.getElementsByName('description')[0].value;
    const category = document.getElementsByName('category')[0].value;
    const image = document.getElementsByName('image')[0].value;

    console.log(category);

    const newProduct = {
      productName,
      price,
      description,
      category,
      image,
    };

    //State
    const products = snapshot(productsList);

    const updatedProductsList = await AddProductUseCase.execute(
      newProduct,
      products.products
    );

    productsList.products = [...updatedProductsList];

    this.cleanInputs(e);
  }

  cleanInputs(e) {
    e.preventDefault();
    document.querySelector('form').reset();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('product-editor', ProductEditor);
