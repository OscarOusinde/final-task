import { LitElement, html } from 'lit';
import { snapshot, subscribe } from 'valtio';
import { productsCategory, singleProduct } from '../states/state';

export class ProductsCategory extends LitElement {
  static get properties() {
    return {
      productsList: { type: Array },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    const productsList = snapshot(productsCategory); //state snap
    this.productsList = productsList.products;

    subscribe(productsCategory, () => {
      const products = snapshot(productsCategory);
      this.productsList = products.products;
    });
  }

  render() {
    return html` <h2 class="productsByCategoryTitle">Products by category</h2>
      <section class="categoriesSection">
        <ul class="categoriesSection__productsList">
          ${this.productsList?.length > 0
            ? this.productsList?.map(
                (product) =>
                  html`<li>
                    <a
                      href="/product"
                      data-product="${JSON.stringify(product)}"
                      @click="${this.gotToProduct}"
                      class="categoriesSection__productsList__linkProduct"
                    >
                      <product-card .product=${product}></product-card>
                    </a>
                  </li>`
              )
            : html`<li class="categoriesSection__productsList__noProducts">
                Not products yet in this category
              </li>`}
        </ul>
      </section>`;
  }

  gotToProduct(e) {
    const product = JSON.parse(e.currentTarget.dataset.product);
    singleProduct.product = product; //state upload
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('products-category', ProductsCategory);
