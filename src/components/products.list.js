import { LitElement, html } from 'lit';
import { snapshot, subscribe } from 'valtio';
import { productsList } from '../states/state';
import { DeleteProductUseCase } from '../usecases/delete-product.usecase';

export class ProductsList extends LitElement {
  static get properties() {
    return {
      productsList: { type: Array },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    //State
    const productsManager = snapshot(productsList);
    this.productsList = productsManager.products;

    subscribe(productsList, () => {
      const productsUpdate = snapshot(productsList);
      this.productsList = productsUpdate.products;
    });
  }

  render() {
    return html`
      <section class="managerList">
        <h2 class="managerList__title">Manager products list</h2>
        <ul class="managerList__productslist">
          ${this.productsList.length > 0
            ? this.productsList?.map(
                (product) => html` <li
                  @click="${this.selectProduct}"
                  data-product="${JSON.stringify(product)}"
                  class="managerList__productslist__product"
                >
                  <p class="managerList__productslist__product__productName">
                    ${product.productName}
                  </p>
                  <button
                    @click="${this.deleteProduct}"
                    data-product="${JSON.stringify(product)}"
                    class="managerList__productslist__product__deleteButton"
                  >
                    Delete
                  </button>
                </li>`
              )
            : html`<li class="managerList__productslist__product__noProducts">
                Not products available. You can create new products in the
                products editor section
              </li>`}
        </ul>
      </section>
    `;
  }

  async deleteProduct(e) {
    const product = JSON.parse(e.currentTarget.dataset.product);

    //State
    const products = snapshot(productsList);

    const updatedList = await DeleteProductUseCase.execute(
      product.id,
      products.products
    );

    //State
    productsList.products = updatedList;

    document.querySelector('form').reset();
  }

  selectProduct(e) {
    const product = JSON.parse(e.currentTarget.dataset.product);

    const productSelected = new CustomEvent('product', {
      bubbles: true,
      composed: true,
      detail: {
        product: product,
      },
    });
    this.dispatchEvent(productSelected);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('products-list', ProductsList);
