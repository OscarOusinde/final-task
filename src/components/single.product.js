import { LitElement, html } from 'lit';
import { singleProduct } from '../states/state';

export class SingleProduct extends LitElement {
  static get properties() {
    return {
      product: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    //State
    this.product = singleProduct.product;
  }

  render() {
    return html`
      <section class="singleProductDisplay">
        <img src="${this.product.image}" class="singleProductDisplay__image" />
        <article class="singleProductDisplay__data">
          <p class="singleProductDisplay__data__productName">
            ${this.product.productName}
          </p>
          <ul class="singleProductDisplay__data__rating">
            <li>
              ${this.product.rating ? this.product.rating : 'No reviews yet'}
            </li>
            <li>${this.product.rating ? 'out of 5 ⭐' : ''}</li>
          </ul>
          <p class="singleProductDisplay__data__productDescription">
            ${this.product.description}
          </p>

          <p class="singleProductDisplay__data__price">
            ${this.product.price} <span>$</span>
          </p>
        </article>
      </section>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('single-product', SingleProduct);
