import './main.css';
import './pages/home.page';
import './pages/products.page';
import './pages/product.page';
import './pages/manager.page';

import { Router } from '@vaadin/router';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);
router.setRoutes([
  { path: '/', component: 'home-page' },
  { path: '/products', component: 'products-page' },
  { path: '/product', component: 'product-page' },
  { path: '/manager', component: 'manager-page' },
  { path: '(.*)', redirect: '/' },
]);
