export class Product {
  constructor({
    id,
    productName,
    price,
    description,
    category,
    image,
    rating,
  }) {
    this.id = id;
    this.productName = productName;
    this.price = price;
    this.description = description;
    this.category = category;
    this.image = image;
    this.rating = rating;
  }
}
