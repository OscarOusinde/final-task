import '../components/header.store';
import '../components/main.store';
import '../components/footer.store';

export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <header-store></header-store>
      <main-store></main-store>
      <footer-store></footer-store>
      `;
  }
}

customElements.define('home-page', HomePage);
