import '../components/header.store';
import '../components/products.list';
import '../components/product.editor';

export class ManagerPage extends HTMLElement {
  constructor() {
    super();
    this.productSelected = {};
  }

  connectedCallback() {
    this.innerHTML = `
        <header-store></header-store>
        <products-list></products-list>
        <product-editor></product-editor>
        <footer-store></footer-store>
        `;

    this.addEventListener('product', (e) => {
      this.productSelected = e.detail.product;

      const productEditor = document.querySelector('product-editor');
      productEditor.productSelected = this.productSelected;
    });
  }
}

customElements.define('manager-page', ManagerPage);
