import '../components/single.product';
import '../components/header.store';
import '../components/footer.store';

export class ProductPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <header-store></header-store>
    <single-product></single-product>
    <footer-store></footer-store>
    `;
  }
}

customElements.define('product-page', ProductPage);
