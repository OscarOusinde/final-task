import '../components/products.category';
import '../components/header.store';
import '../components/footer.store';

export class ProductsPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <header-store></header-store>
      <products-category></products-category>
      <footer-store></footer-store>
      `;
  }
}

customElements.define('products-page', ProductsPage);
