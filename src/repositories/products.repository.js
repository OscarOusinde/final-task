import axios from 'axios';

export class ProductsRepository {
  async getAllProducts() {
    return await (
      await axios.get('https://fakestoreapi.com/products')
    ).data;
  }

  async addNewProduct(newProduct) {
    const product = {
      title: newProduct.productName,
      price: newProduct.price,
      description: newProduct.description,
      image: newProduct.image,
      category: newProduct.category,
    };
    return await (
      await axios.post(`https://fakestoreapi.com/products`, product)
    ).data;
  }
}
