import { proxy, subscribe } from 'valtio';

const productsList = proxy({ products: [] });
const productsCategory = proxy({ products: [] });
const singleProduct = proxy({ product: [] });

//LS productsList
const productsListLS = JSON.parse(localStorage.getItem('productsList'));
if (productsListLS) productsList.products = productsListLS.products;

subscribe(productsList, () => {
  localStorage.setItem('productsList', JSON.stringify(productsList));
});

//LS productsCategory
const productsCategoryLS = JSON.parse(localStorage.getItem('productsCategory'));
if (productsCategoryLS) productsCategory.products = productsCategoryLS.products;

subscribe(productsCategory, () => {
  localStorage.setItem('productsCategory', JSON.stringify(productsCategory));
});

//LS singleProduct
const singleProductLS = JSON.parse(localStorage.getItem('singleProduct'));
if (singleProductLS) singleProduct.product = singleProductLS.product;

subscribe(singleProduct, () => {
  localStorage.setItem('singleProduct', JSON.stringify(singleProduct));
});

export { productsCategory, productsList, singleProduct };
