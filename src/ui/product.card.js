import { LitElement, html } from 'lit';

export class ProductCard extends LitElement {
  static get properties() {
    return {
      product: { type: Object },
    };
  }

  render() {
    return html`<article
      class="mainStore__productsList__linkProduct__cardProduct"
    >
      <img
        src="${this.product.image}"
        class="mainStore__productsList__linkProduct__cardProduct__image"
      />
      <p class="mainStore__productsList__linkProduct__cardProduct__productName">
        ${this.product.productName}
      </p>
      <span
        class="mainStore__productsList__linkProduct__cardProduct__productPrice"
        >${this.product.price} $</span
      >
    </article>`;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('product-card', ProductCard);
