import { Product } from '../model/product';
import { ProductsRepository } from '../repositories/products.repository';

export class AddProductUseCase {
  static async execute(newProduct, productList) {
    const repository = new ProductsRepository();
    const productAdded = await repository.addNewProduct(newProduct);

    //Check the length of the actual products list and add +1 to calculate
    //the id of the new added product, beacause the API always returns the same id.
    const idNewProduct = productList.length + 1;

    const product = new Product({
      id: idNewProduct,
      productName: productAdded.title,
      price: productAdded.price,
      description: productAdded.description,
      category: productAdded.category,
      image: productAdded.image,
      rating: 0,
    });

    return [product, ...productList];
  }
}
