import { ProductsRepository } from '../repositories/products.repository';
import { Product } from '../model/product';

export class AllProductsUseCase {
  static async execute() {
    const repository = new ProductsRepository();
    const products = await repository.getAllProducts();

    return products.map(
      (product) =>
        new Product({
          id: product.id,
          productName: product.title,
          price: product.price,
          description: product.description,
          category: product.category,
          image: product.image,
          rating: product.rating.rate,
        })
    );
  }
}
