export class DeleteProductUseCase {
  static execute(productId, productList) {
    return productList.filter((product) => product.id !== productId);
  }
}
