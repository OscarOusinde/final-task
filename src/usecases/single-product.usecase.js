export class SingleProductUseCase {
  static execute(productId, productList) {
    return productList.filter((product) => product.id === productId);
  }
}
