export class UpdateProductUseCase {
  static execute(productUpdate, productList) {
    return productList.map((product) =>
      product.id === productUpdate.id ? (product = productUpdate) : product
    );
  }
}
