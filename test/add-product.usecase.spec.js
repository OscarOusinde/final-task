import { ProductsRepository } from '../src/repositories/products.repository';
import { AddProductUseCase } from '../src/usecases/add-product.usecase';

jest.mock('../src/repositories/products.repository');
describe('Should add to the product list', () => {
  beforeEach(() => {
    ProductsRepository.mockClear();
  });
  it('a new product', async () => {
    const newProduct = {
      productName: 'test product',
      price: 18,
      description: 'lorem ipsum test',
      image:
        'https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE4LqQX?ver=fe80&q=90&m=6&h=705&w=1253&b=%23FFFFFFFF&f=jpg&o=f&p=140&aim=true',
      category: 'electronic',
    };

    ProductsRepository.mockImplementation(() => {
      return {
        addNewProduct: () => {
          return {
            id: 21,
            title: newProduct.productName,
            price: newProduct.price,
            category: newProduct.category,
            description: newProduct.description,
            image: newProduct.image,
          };
        },
      };
    });

    const productList = [
      {
        id: 1,
        productName: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 109.95,
        description:
          'Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday',
        category: "men's clothing",
        image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        rating: 3.9,
      },
      {
        id: 2,
        productName: 'Mens Casual Premium Slim Fit T-Shirts ',
        price: 22.3,
        description:
          'Slim-fitting style, contrast raglan long sleeve, three-button henley placket, light weight & soft fabric for breathable and comfortable wearing. And Solid stitched shirts with round neck made for durability and a great fit for casual fashion wear and diehard baseball fans. The Henley style round neckline includes a three-button placket.',
        category: "men's clothing",
        image:
          'https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg',
        rating: 4.1,
      },
      {
        id: 3,
        productName: 'Mens Cotton Jacket',
        price: 55.99,
        description:
          'great outerwear jackets for Spring/Autumn/Winter, suitable for many occasions, such as working, hiking, camping, mountain/rock climbing, cycling, traveling or other outdoors. Good gift choice for you or your family member. A warm hearted love to Father, husband or son in this thanksgiving or Christmas Day.',
        category: "men's clothing",
        image: 'https://fakestoreapi.com/img/71li-ujtlUL._AC_UX679_.jpg',
        rating: 4.7,
      },
    ];

    const updatedProductList = await AddProductUseCase.execute(
      newProduct,
      productList
    );

    expect(updatedProductList.length).toBe(productList.length + 1);
    expect(updatedProductList[0].title).toBe(newProduct.title);
  });
});
