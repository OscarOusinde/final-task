import { ProductsRepository } from '../src/repositories/products.repository';
import { AllProductsUseCase } from '../src/usecases/all-products.usecase';
import { PRODUCTS } from '../test/fixtures/products';

jest.mock('../src/repositories/products.repository');

describe('All products use case', () => {
  beforeEach(() => {
    ProductsRepository.mockClear();
  });

  it('should get all products', async () => {
    ProductsRepository.mockImplementation(() => {
      return {
        getAllProducts: () => {
          return PRODUCTS;
        },
      };
    });

    const products = await AllProductsUseCase.execute();

    expect(products.length).toBe(20);
    expect(products[0].productName).toBe(PRODUCTS[0].title);
    expect(products[0].price).toBe(PRODUCTS[0].price);
  });
});
