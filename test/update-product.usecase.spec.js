import { UpdateProductUseCase } from '../src/usecases/update-product.usecase';

describe('Update a product', () => {
  it('should update a product characteristics', () => {
    const productUpdate = {
      id: 1,
      productName: 'Name updated',
      price: 1,
      description: 'Description updated',
      category: 'Category updated',
      image: 'https://images.vans.com/is/image/VansEU/VN0A3UI6BLK-HERO',
      rating: 2.5,
    };

    const productList = [
      {
        id: 1,
        productName: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 109.95,
        description:
          'Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday',
        category: "men's clothing",
        image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        rating: 3.9,
      },
      {
        id: 2,
        productName: 'Mens Casual Premium Slim Fit T-Shirts ',
        price: 22.3,
        description:
          'Slim-fitting style, contrast raglan long sleeve, three-button henley placket, light weight & soft fabric for breathable and comfortable wearing. And Solid stitched shirts with round neck made for durability and a great fit for casual fashion wear and diehard baseball fans. The Henley style round neckline includes a three-button placket.',
        category: "men's clothing",
        image:
          'https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg',
        rating: 4.1,
      },
      {
        id: 3,
        productName: 'Mens Cotton Jacket',
        price: 55.99,
        description:
          'great outerwear jackets for Spring/Autumn/Winter, suitable for many occasions, such as working, hiking, camping, mountain/rock climbing, cycling, traveling or other outdoors. Good gift choice for you or your family member. A warm hearted love to Father, husband or son in this thanksgiving or Christmas Day.',
        category: "men's clothing",
        image: 'https://fakestoreapi.com/img/71li-ujtlUL._AC_UX679_.jpg',
        rating: 4.7,
      },
    ];

    const updatedProductList = UpdateProductUseCase.execute(
      productUpdate,
      productList
    );

    console.log(updatedProductList);

    expect(updatedProductList.length).toBe(productList.length);
    expect(updatedProductList[0].productName).toBe(productUpdate.productName);
    expect(updatedProductList[0].price).toBe(productUpdate.price);
    expect(updatedProductList[0].description).toBe(productUpdate.description);
    expect(updatedProductList[0].category).toBe(productUpdate.category);
    expect(updatedProductList[0].image).toBe(productUpdate.image);
    expect(updatedProductList[0].rating).toBe(productUpdate.rating);
  });
});
